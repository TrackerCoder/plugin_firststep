#include "MyObjectFactory.h"
#include "MyObject.h"

#define LOCTEXT_NAMESPACE "UMyObjectFactory"

UMyObjectFactory::UMyObjectFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UMyObject::StaticClass();
}

UMyObjectFactory::UMyObjectFactory()
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UMyObject::StaticClass();
}

UObject* UMyObjectFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	return NewObject<UMyObject>(InParent, Class, Name, Flags | RF_Transactional);
}

#undef LOCTEXT_NAMESPACE
