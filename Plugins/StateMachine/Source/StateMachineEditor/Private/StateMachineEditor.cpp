
#include "StateMachineEditor.h"

#define LOCTEXT_NAMESPACE "FStateMachineEditorModule"

void FStateMachineEditorModule::StartupModule()
{
}

void FStateMachineEditorModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FStateMachineEditorModule, StateMachineEditor)
