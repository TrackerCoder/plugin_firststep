// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class StateMachineEditor : ModuleRules
{
	public StateMachineEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        //PublicIncludePaths.AddRange(
        //    new string[] {
        //        "StateMachineEditor/Public"
        //    }
        //    );


        //PrivateIncludePaths.AddRange(
        //    new string[] {
        //        "StateMachineEditor/Private"
        //    }
        //    );


        PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"StateMachine",
			}
			);

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"CoreUObject",
				"Engine",
				"InputCore",
				"Slate",
				"SlateCore",
				"UnrealEd",
                "PropertyEditor",
				"RenderCore",
				"ContentBrowser",
				"WorkspaceMenuStructure",
				"EditorStyle",
				"EditorWidgets",
				"Projects",
				"AssetRegistry",
				"InputCore"
				// ... add private dependencies that you statically link with here ...
			});



		PrivateIncludePathModuleNames.AddRange(
			new string[] {
              //  "UnrealEd",
                "Settings",
				"IntroTutorials",
				"AssetTools",
				"LevelEditor"
			});

		//PrivateDependencyModuleNames.AddRange(
		//	new string[]
		//	{
		//		"CoreUObject",
		//		"Engine",
		//		"Slate",
		//		"SlateCore",
		//		// ... add private dependencies that you statically link with here ...	
		//	}
		//	);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				"AssetTools",
			}
			);
	}
}
