#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "MyObjectFactory.generated.h"

UCLASS()
class STATEMACHINE_API UMyObjectFactory : public UFactory
{
	GENERATED_UCLASS_BODY()

	UMyObjectFactory();

	// UFactory interface
	virtual UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
};
