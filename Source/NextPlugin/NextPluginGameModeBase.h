// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NextPluginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class NEXTPLUGIN_API ANextPluginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
