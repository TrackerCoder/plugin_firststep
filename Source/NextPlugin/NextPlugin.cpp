// Copyright Epic Games, Inc. All Rights Reserved.

#include "NextPlugin.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NextPlugin, "NextPlugin" );
